const navButton = document.querySelector(".nav-container");
const navContainer = document.querySelector(".button-container");

navButton.addEventListener("click", () => {
    navButton.classList.toggle("nav-container--active");
    navContainer.classList.toggle("button-container--active");
});